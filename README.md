# DCF77 time protocol demo setup

Shows how the German time signal encodes data to synchronize clocks in central Europe.

The bits are output by an Arduino Nano to a NeoPixel LED strip. For details on the DCF77 protocol see https://dcf77logs.de/dcf77
